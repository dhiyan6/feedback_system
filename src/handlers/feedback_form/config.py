#define all the config details required for feedback form handler

feedback_form_template = "feedback/feedback_form.html"
feedback_form_thankyou_template = "feedback/feedback_thankyou_response.html"
feedback_individual_response = "feedback/feedback_individual_response.html"
feedback_all_response = "feedback/feedback_all_response.html"


#Schema definition for post request
required_fields_feedback_response = ('name', 'email', 'starValue', 'comments')

feedback_request_schema = {
        "type" : "object",
        "properties" : {
        "starValue" : {"type" : "string" ,"minLength": 1 ,"maxLength": 2 ,"pattern":"[1-5]$"},
        "name" : {"type" : "string","minLength": 2,},
        "email" : {"type" : "string","minLength": 2,"pattern":"[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$"},
        "comments" : {"type" : "string","minLength": 2,},
        },
        "required": ["starValue", "name", "email", "comments"]
    }