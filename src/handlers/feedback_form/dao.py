from datetime import datetime
import traceback
import tornado.web

from src.conf.logger_conf import configure_logger
from src.utils.db_utils import query_db
logger = configure_logger("feedback_form")


class FeedBackResponseDAO(object):

    async def get(self,feedback_id):
        logger.info('')
        try:
            query = "select * from feedback where feedback_id = ?"
            response = await query_db(query, args=(feedback_id,),one=True)
            if response:
                return response
            else:
                raise tornado.web.HTTPError(status_code=404, log_message="FeedbackID not found")
        except Exception as e:
            logger.error('Error while getting feedback from db meassage - {0} - traceback-{1}'.format(e,traceback.format_exc()))
            raise e

    async def get_all(self):
        logger.info('')
        try:
            query = "select * from feedback"
            response = await query_db(query)
            return response if response else []
        except Exception as e:
            logger.error('Error while getting feedback from db meassage - {0} - traceback-{1}'.format(e,
                                                                                                        traceback.format_exc()))
            raise e

    async def post(self, feedback_data):

        try:
            logger.info('')
            query = "insert into feedback ({0},{2},{4},{6},{8}) values('{1}','{3}','{5}','{7}','{9}')".format(
                "name",feedback_data["name"],
                "email",feedback_data["email"],
                "rating",feedback_data["starValue"],
                "comments",feedback_data["comments"],
                "created_on",datetime.now()
            )
            logger.debug(f'query {query}')
            query_result = await query_db(query,insert=True)
            logger.debug(f'query {query_result}')
            return query_result
        except Exception as e:
            logger.error('Error while saving feedback to db meassage - {0} - traceback-{1}'.format(e, traceback.format_exc()))
            raise e


