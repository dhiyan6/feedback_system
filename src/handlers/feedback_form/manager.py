import traceback

from src.handlers.feedback_form.config import feedback_form_thankyou_template, feedback_individual_response, \
    feedback_all_response
from src.handlers.feedback_form.dao import FeedBackResponseDAO
from src.conf.logger_conf import configure_logger
logger = configure_logger("feedback_form")

class FeedBackResponseManager(object):
    async def get(self, feedback_id=None):
        """
        Method to get the feedback response given feedback_id
        :param feedback_id: feedback_id to be retrived
        :return: returns the template name and feedback_Response
        """
        try:

            logger.info('')
            if feedback_id:
                feedback_response = await FeedBackResponseDAO().get(feedback_id)
                template_name = feedback_individual_response
            else:
                feedback_response = await FeedBackResponseDAO().get_all()
                template_name = feedback_all_response
            response = {"temlpate_name": template_name, "feedback_response": feedback_response}
            logger.info('')
            return response

        except Exception as e:
            logger.error('Error while getting feedback response from db meassage - {0} - traceback-{1}'.format(e,traceback.format_exc()))
            raise e

    async def post(self, feedback_data):
        """
        Method to post the feedback to the database
        :param feedback_data: feedback data from request
        :return: returns temlpate name and feedback_id
        """
        try:

            logger.info('')
            feedback_id = await FeedBackResponseDAO().post(feedback_data)
            response = {"temlpate_name":feedback_form_thankyou_template,"feedback_id":feedback_id}
            logger.info('')
            return response

        except Exception as e:
            logger.error('Error while posting feedback to db meassage - {0} - traceback-{1}'.format(e,traceback.format_exc()))
            raise e