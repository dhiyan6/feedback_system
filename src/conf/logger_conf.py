import logging
import logging.config

from src.utils.logger_filter import ContextFilter

def configure_logger(name):
    logging.config.dictConfig({
        'version': 1,
        'filters': {
            'rid_filter': {
                '()': ContextFilter,
            }
        },
        'formatters': {
            'default': {'format': '%(r_id)s - %(asctime)s - %(name)s - %(pathname)s - %(lineno)d - %(levelname)s - %(message)s', 'datefmt': '%Y-%m-%d %H:%M:%S'},
            'access': {'format': '%(r_id)s - %(asctime)s - %(name)s  - %(levelname)s - %(message)s', 'datefmt': '%Y-%m-%d %H:%M:%S'}
        },
        'handlers': {
            'file': {
                'level': 'DEBUG',
                'class': 'logging.handlers.TimedRotatingFileHandler',
                'filters': ['rid_filter'],
                'formatter': 'default',
                'filename': '/logs/application.log',
                'when':'D',
                'interval':1,
                'backupCount': 5
            },
            'file_root': {
                'level': 'DEBUG',
                'class': 'logging.handlers.TimedRotatingFileHandler',
                'filters': ['rid_filter'],
                'formatter': 'access',
                'filename': '/logs/access.log',
                'when': 'D',
                'interval': 1,
                'backupCount': 5
            }
        },
        'loggers': {
            'feedback_form': {
                'level': 'DEBUG',
                'handlers': ['file'],
                "propagate": 0
            },
            '': {
                'level': 'DEBUG',
                'handlers': ['file_root'],
                "propagate": 0
            },
        },
        'disable_existing_loggers': False
    })
    return logging.getLogger(name)