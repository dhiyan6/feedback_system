from datetime import datetime
import traceback
from tornado.ioloop import IOLoop

from src.conf.logger_conf import configure_logger
from src.utils.db_utils import query_db
from src.utils.minio_utils import Minio_Utils

logger = configure_logger("feedback_form")

"""
Schedule this task in cron table daily, so after the log rotation happens, the log file will be uploaded to db. 
"""

async def upload_log():
    try:
        logger.info('')
        now = datetime.now()
        date_format = now.strftime("%Y-%m-%d")
        log_location = f'/logs/application.log.{date_format}'
        log_reference = Minio_Utils().upload_file(log_location)
        await update_db(log_location,log_reference)
    except Exception as e:
        logger.error(
            'Error while uploading log file meassage - {0} - traceback-{1}'.format(e, traceback.format_exc()))
        raise e

async def update_db(log_location,log_reference):
    try:
        logger.info('')
        query = "insert into log_minio_reference ({0},{2},{4}) values('{1}','{3}','{5}')".format(
            "log_location", log_location,
            "log_reference", log_reference,
            "created_on", datetime.now()
        )
        logger.debug(f'query {query}')
        query_result = await query_db(query, insert=True)
        logger.debug(f'query {query_result}')
        return query_result
    except Exception as e:
        logger.error(
            'Error while updating log reference to db meassage - {0} - traceback-{1}'.format(e, traceback.format_exc()))
        raise e

if __name__ == "__main__":


    io_loop = IOLoop.current()
    io_loop.run_sync(upload_log)








