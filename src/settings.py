#Define all the application settings here
import os
from src.handlers.base_handler import BaseHandler,NotFound404Handler


DATABASE = '/db/feedback.db'
log_file_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'conf/logging.ini')
static_file_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'static')
template_file_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'templates')


application_settings = {
    "static_path":static_file_path,
    "template_path":template_file_path,
    "debug":True,
    "default_handler_class":NotFound404Handler
}
