from minio import Minio
from minio.error import (ResponseError, BucketAlreadyOwnedByYou,
                         BucketAlreadyExists)

from src.scripts.settings import log_storage,log_storage_bucket_name

class Minio_Utils(object):

    def __init__(self):

        self.log_storage = Minio(f"{log_storage['host']}:{log_storage['port']}",
                    access_key=log_storage['log_storage_access_key'],
                    secret_key=log_storage['log_storage_secret_key'],
                                 secure=False
                    )
        self.bucket_name = log_storage_bucket_name

    def make_bucket(self, bucket_name):

        if not self.log_storage.bucket_exists(self.bucket_name):
            self.log_storage.make_bucket(self.bucket_name)


    def upload_file(self,file_path='/logs/application_1.log'):

        self.make_bucket(self.bucket_name)
        log_reference = self.log_storage.fput_object(self.bucket_name,file_path.split('/')[-1],file_path)
        return log_reference


if __name__ == "__main__":

    Minio_Utils().upload_file()
