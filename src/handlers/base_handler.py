import tornado.web

class BaseHandler(tornado.web.RequestHandler):
    """
    Base handler gonna to be used instead of RequestHandler,
    to handle http errors
    """
    def write_error(self, status_code, **kwargs):
        if status_code in [400, 403, 404, 500, 503]:
            self.render(f'{status_code}.html')


class NotFound404Handler(tornado.web.RequestHandler):
    # Override prepare() instead of get() to cover all possible HTTP methods.
    def prepare(self):
        self.set_status(404)
        self.render("404.html")