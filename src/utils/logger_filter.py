import contextvars
import functools
import logging
import uuid

var = contextvars.ContextVar('r_id')

class ContextFilter(logging.Filter):
    """
    This is a filter which injects contextual information into the log.

    Rather than use actual contextual information, we just use random
    data in this demo.
    """

    def filter(self, record):

        record.r_id = var.get(0)
        return True



def set_request_id(func):

    """
        Check for request header before setting the request_id, If available in the request use the same. This
        will come into play if we are having many services and we are calling one service from other, so we can track
        the whole work flow usign a sing request_id
    """

    @functools.wraps(func)
    def inner(args,**kwargs):
        var.set(uuid.uuid4())
        func(args,**kwargs)

    return inner


def async_set_request_id(func):

    """
        Check for request header before setting the request_id, If available in the request use the same. This
        will come into play if we are having many services and we are calling one service from other, so we can track
        the whole work flow usign a sing request_id
    """

    @functools.wraps(func)
    async def inner(args, **kwargs):
        var.set(uuid.uuid4())
        return await func(args, **kwargs)
    return inner