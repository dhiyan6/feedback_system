FROM python:3.7

EXPOSE 8888

RUN apt-get update && \
      apt-get -y install sudo

RUN sudo apt-get install -y sqlite3 libsqlite3-dev
RUN mkdir /db
RUN touch /db/feedback.db

RUN mkdir -p /logs

RUN mkdir -p /app/src
WORKDIR /app

COPY requirements.txt /app
RUN pip install --no-cache-dir -r requirements.txt

ADD src /app/src/

RUN sqlite3 /db/feedback.db < /app/src/database/schema.sql

ENV PYTHONPATH "${PYTHONPATH}:/app"

#ENTRYPOINT ["tail", "-f", "/dev/null"]
#ENTRYPOINT ["python3", "src/app.py"]
WORKDIR /app
