# Feedback System

## What it does?

A web application written in tornado helps the customers to provide feedback.

## How to run/deploy the system
    If there are no changes then do the following steps
    
        git clone https://dhiyan6@bitbucket.org/dhiyan6/feedback_system.git
        cd feedback_system/
        docker-compose up
    
    If you are adding changes then do the following steps
        
        Then run: docker-compose up --build

## How to access the system

    To Add a feedback: http://127.0.0.1:8888/feedbackform
    To View all feedback given : http://127.0.0.1:8888/feedbackresponse
    To View a single feedback : http://127.0.0.1:8888/feedbackresponse?feedback_id=<feedback_id>
    To view MINIO web ui: http://127.0.0.1:9000/minio/login
        Access Key : minioadmin , Secret Key : minioadmin
        
    To Access the API:
        127.0.01:8888/api/feedbackresponse
        http://127.0.0.1:8888/api/feedbackresponse?feedback_id=<feedback_id>
                    
## How to Debug

    Both application and access logs will be avaible at /logs. All the log entries will have a
    request id , so use the request id to track the application flow. Request id is unique to 
    a request.
    
## What Database Stores

    We are using SQLlite to store the data. There will be two tables. Feedback table contains
    information about the feedback given by the customer. log_minio_reference table will have
    details about the logs location and MINIO reference.
    
## Which Tech Stack Used           
    
        Python
        Tornado: Web Framework
        SQLite: Database
        MINIO server: To store logs 
        
## Project Structure
    
    The src directory has handler directory, it is the home for all the handlers available in the system.
    Each service will have its own handler directory like feedback service has feedback_form. Each handler
    directory has three main components namely handler, manager and DAO. Handler.py will receive the http
    request and will vlidate the request data, if everythiing is ok it will call the respective manager
    to process the request. manager.py will have all the business logic and will call respectove DAO to
    access the database.
    
# Application Container
    # To build the application image: sudo docker build Dockerfile -t  feedback_system:latest .
    # To run application container alone :  sudo docker run -d -p 80:8888 --name application_feedback feedback_system
    # To Start and stop the container:  sudo docker start application_feedback and sudo docker stop application_feedback
    # To get into container : sudo docker exec -it application_feedback /bin/bash
    
#MINIO Container
    #To run MINIO container alone : docker run -d -p 9000:9000 minio/minio server /data    