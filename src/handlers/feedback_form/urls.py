#defie all the urls tuple of feedback handler here

from src.handlers.feedback_form.handler import FeedBackResponseHandler, FeedBackFormHandler, FeedAPIBackResponseHandler

feedback_form_handler_urls = [
        (r'/',FeedBackFormHandler),
        (r'/feedbackform',FeedBackFormHandler),
        (r'/feedbackresponse',FeedBackResponseHandler),
        (r'/api/feedbackresponse/?',FeedAPIBackResponseHandler),
        (r'/api/feedbackresponse/feedback_id/(?P<feedback_id>\w+)',FeedAPIBackResponseHandler),

    ]