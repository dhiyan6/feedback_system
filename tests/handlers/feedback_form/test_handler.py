from bs4 import BeautifulSoup
import tornado
import unittest
from tornado.testing import  AsyncTestCase, AsyncHTTPClient,gen_test
from tornado.httpclient import AsyncHTTPClient
import json

from src.handlers.feedback_form.config import required_fields_feedback_response


class MyTestCase(AsyncTestCase):

    @tornado.testing.gen_test
    def test_http_fetch_form(self):
        client = AsyncHTTPClient()
        response = yield client.fetch("http://127.0.0.1:8888")
        # Test contents of response
        soup = BeautifulSoup(response.body, 'html5lib')
        assert 1 == len(soup.findAll("input", {"id": "name","placeholder":"Your Name *"}))
        assert 1 == len(soup.findAll("input", {"id": "email","placeholder":"Your Email *"}))
        assert 1 == len(soup.findAll("textarea", {"placeholder":"Your Message *"}))
        assert 1 == len(soup.findAll("input", {"name":"starValue","value":"1"}))
        assert 1 == len(soup.findAll("input", {"name":"starValue","value":"2"}))
        assert 1 == len(soup.findAll("input", {"name":"starValue","value":"3"}))
        assert 1 == len(soup.findAll("input", {"name":"starValue","value":"4"}))
        assert 1 == len(soup.findAll("input", {"name":"starValue","value":"5"}))

        response = yield client.fetch("http://127.0.0.1:8888/feedbackform")
        # Test contents of response
        soup = BeautifulSoup(response.body, 'html5lib')

        assert 1 == len(soup.findAll("input", {"id": "name", "placeholder": "Your Name *"}))
        assert 1 == len(soup.findAll("input", {"id": "email", "placeholder": "Your Email *"}))
        assert 1 == len(soup.findAll("textarea", {"placeholder": "Your Message *"}))
        assert 1 == len(soup.findAll("input", {"name": "starValue", "value": "1"}))
        assert 1 == len(soup.findAll("input", {"name": "starValue", "value": "2"}))
        assert 1 == len(soup.findAll("input", {"name": "starValue", "value": "3"}))
        assert 1 == len(soup.findAll("input", {"name": "starValue", "value": "4"}))
        assert 1 == len(soup.findAll("input", {"name": "starValue", "value": "5"}))

    @tornado.testing.gen_test
    def test_feedback_post(self):

        client = AsyncHTTPClient()

        request_data = {"name": "dhiyaneshwar c", "email": "dhiyan6@gmail.com", "starValue": "4", "comments": "testing"}
        request_data_dumps = json.dumps(request_data)

        response = yield client.fetch("http://127.0.0.1:8888/api/feedbackresponse",method= "POST", body=request_data_dumps)
        feedback_id = json.loads(response.body)['response']['feedback_id']
        assert type(feedback_id) is  int

        response = yield client.fetch(f"http://127.0.0.1:8888/api/feedbackresponse/feedback_id/{feedback_id}")
        response_feedback = json.loads(response.body)['response']
        for key in required_fields_feedback_response:
            if key == 'starValue':
                assert int(request_data[key]) == response_feedback['rating']
            else:
                assert request_data[key] == response_feedback[key]

    @tornado.testing.gen_test
    def test_400_post_request(self):

        try:
            client = AsyncHTTPClient()
            request_data = {"name": "dhiyaneshwar c", "email": "dhiyan6@gmail.com", "comments": "testing"}
            request_data_dumps = json.dumps(request_data)
            response = yield client.fetch("http://127.0.0.1:8888/api/feedbackresponse", method="POST",
                                          body=request_data_dumps)
        except Exception as e:
            assert type(e) == tornado.httpclient.HTTPClientError

    @tornado.testing.gen_test
    def test_feedback_get_all(self):

        client = AsyncHTTPClient()

        response = yield client.fetch(f"http://127.0.0.1:8888/api/feedbackresponse/")
        response_feedback = json.loads(response.body)['response']
        assert type(json.loads(response.body)['response']) is list

unittest.main()