DROP TABLE IF EXISTS feedback;
CREATE TABLE feedback(
   feedback_id INTEGER PRIMARY KEY AUTOINCREMENT,
   name  VARCHAR(255) NOT NULL,
   email VARCHAR(100) NOT NULL,
   rating Integer NOT NULL,
   comments TEXT,
   created_on TEXT
   );

DROP TABLE IF EXISTS log_minio_reference;
CREATE TABLE log_minio_reference(
   log_id INTEGER PRIMARY KEY AUTOINCREMENT,
   log_location  VARCHAR(255) NOT NULL,
   log_reference VARCHAR(100) NOT NULL,
   created_on TEXT
   );