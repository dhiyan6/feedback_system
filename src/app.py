import tornado.web
import tornado.ioloop


from src.settings import application_settings
from src.urls import application_urls

def make_app():
    application = tornado.web.Application(
        application_urls,
        **application_settings)
    return application

if __name__ == "__main__":

    app = make_app()
    app.listen(8888)
    tornado.ioloop.IOLoop.current().start()