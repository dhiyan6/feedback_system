import aiosqlite
import traceback

from src.conf.logger_conf import configure_logger
from src.settings import DATABASE


logger = configure_logger("feedback_form")


async def get_db():
    """
        The will create a db connection and returns it
    :return:db_connection
    """
    try:
        logger.info('')
        db = await aiosqlite.connect(DATABASE)
        def make_dicts(cursor, row):
            return dict((cursor.description[idx][0], value)
                        for idx, value in enumerate(row))

        db.row_factory = make_dicts

        logger.info('')
        return db
    except Exception as e:
        logger.error(
            'Error while getting db meassage - {0} - traceback-{1}'.format(e, traceback.format_exc()))
        raise e

async def query_db(query, args=(), one=False, insert = False):

    """
    Method to execute sql queries
    :param query:sql query
    :param args: args for query
    :param one: True if query is expected to retuen one row
    :param insert: True if query is expected to insert record to db
    :param insert: True if query is expected to insert record to db
    :return:
    """
    try:
        logger.info('')
        logger.debug("query = {0}".format(query))
        db_con = await get_db()
        cur = await db_con.execute(query, args)
        if insert:
            rv = cur.lastrowid
        else:
            if one:
                rv = await cur.fetchone()
            else:
                rv = await cur.fetchall()
        await db_con.commit()
        await cur.close()
        await db_con.close()
        logger.debug("query response = {0}".format(rv))
        return rv
    except Exception as e:
        logger.error(
            'Error while querying db meassage - {0} - traceback-{1}'.format(e, traceback.format_exc()))
        raise e
