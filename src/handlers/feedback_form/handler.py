import json
import jsonschema
from jsonschema import validate
import traceback
import tornado

from src.utils.logger_filter import ContextFilter,set_request_id,async_set_request_id
from src.handlers.base_handler import BaseHandler
from src.handlers.feedback_form.config import feedback_form_template,required_fields_feedback_response,feedback_request_schema
from src.handlers.feedback_form.manager import FeedBackResponseManager
from src.conf.logger_conf import configure_logger
logger = configure_logger("feedback_form")

class FeedBackFormHandler(BaseHandler):
    @set_request_id
    def get(self):
        """
        Method to render feedback form template
        :return:
        """
        logger.info('')
        self.render(feedback_form_template)
        logger.info('')


class FeedAPIBackResponseHandler(BaseHandler):
    @async_set_request_id
    async def get(self,feedback_id = None):
        """
        Method to retrive feedback response from database
        :return: feedback response
        """
        try:

            logger.info('')
            logger.debug("request data {}".format(feedback_id))

            response = await FeedBackResponseManager().get(feedback_id)
            logger.debug('response {}'.format(json.dumps(response)))
            logger.info('')
            self.write({"response":response.get("feedback_response")})

        except Exception as e:
            logger.error('Error while getting feedback response from db meassage - {0} - traceback-{1}'.format(e,traceback.format_exc()))
            self.set_status(500)
            self.write({"response": {"message": str(e)}})

    @async_set_request_id
    async def post(self):
        """
        Method to post the feedback response to the database
        :return: feedback id
        """
        try:

            logger.info('')
            request_data = json.loads(self.request.body)

            logger.debug("request data {}".format(json.dumps(request_data)))
            validate(instance=request_data, schema=feedback_request_schema)
            response = await FeedBackResponseManager().post(request_data)
            logger.debug('response {}'.format(json.dumps(response)))
            logger.info('')
            self.write({"response":{"feedback_id":response.get("feedback_id")}})

        except jsonschema.exceptions.ValidationError as e:
            logger.error(
                'Not a valid request input - {0} - traceback-{1}'.format(e, traceback.format_exc()))
            self.set_status(400)
            self.write({"response": {"message": str(e.message)}})

        except Exception as e:
            logger.error('Error while posting feedback to db meassage - {0} - traceback-{1}'.format(e,traceback.format_exc()))
            self.set_status(500)
            self.write({"response": {"message": str(e)}})


class FeedBackResponseHandler(BaseHandler):
    @async_set_request_id
    async def get(self):
        """
        Method to retrive feedback response from database
        :return: renders the template with feedback response
        """
        try:

            logger.info('')
            request_data = {arg: self.get_query_arguments(arg)[0] for arg in self.request.arguments}
            logger.debug("request data {}".format(json.dumps(request_data)))

            response = await FeedBackResponseManager().get(request_data.get('feedback_id'))
            logger.debug('response {}'.format(json.dumps(response)))
            await self.render(response['temlpate_name'], feedback_response=response['feedback_response'])
            logger.info('')

        except Exception as e:
            logger.error('Error while getting feedback response from db meassage - {0} - traceback-{1}'.format(e,traceback.format_exc()))
            raise e

    @async_set_request_id
    async def post(self):
        """
        Method to post the feedback response to the database
        :return: render the template with feedback id
        """
        try:

            logger.info('')
            request_data = {arg: self.get_body_argument(arg) for arg in required_fields_feedback_response}

            logger.debug("request data {}".format(json.dumps(request_data)))
            validate(instance=request_data, schema=feedback_request_schema)
            response = await FeedBackResponseManager().post(request_data)
            logger.debug('response {}'.format(json.dumps(response)))
            await self.render(response['temlpate_name'], feedback_id=response['feedback_id'])
            logger.info('')

        except jsonschema.exceptions.ValidationError as e:
            logger.error(
                'Not a valid request input - {0} - traceback-{1}'.format(e, traceback.format_exc()))
            raise tornado.web.HTTPError(status_code=400, log_message="Not a valid request input")

        except Exception as e:
            logger.error('Error while posting feedback to db meassage - {0} - traceback-{1}'.format(e,traceback.format_exc()))
            raise e