import os

log_storage = {
        "log_storage_access_key": 'minioadmin',
        "log_storage_secret_key": 'minioadmin',
        "host": os.getenv("minio_host",'127.0.0.1'),
        "port": 9000,
        }

log_storage_bucket_name = "application-logs"